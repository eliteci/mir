<?php
use App\Models\Vehicule;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VehiculeController;
use Illuminate\Database\Eloquent\SoftDeletes;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});
Route::get('/edit', function () {
    return view('edit');
});

Route::get('/edit', function () {
    
    $data = \App\Models\Vehicule::all();


    return view('edit', compact('data'));
});

Route::get('show', function () {

    return view('show');
});
Route::get('/show', function () {
    
    $data = \App\Models\Vehicule::all();
   // $data= Vehicule:: withTrashed()->where('id' ,  1)->restore();
    //dd($data);

    return view('show', compact('data'));
});

Route::get('/allVehicule', function () {
    
    $data = \App\Models\Vehicule::all();
   // $data= Vehicule:: withTrashed()->where('id' ,  1)->restore();
    //dd($data);

    return view('allVehicule', compact('data'));
});

 Route::post('/allVehicule',   [VehiculeController::class, 'store'])->name('Vehicule.store');



Route::get('Vehicule.search',   [VehiculeController::class, 'research'])->name('Vehicule.research');

Route::get('/Vehicule',   [VehiculeController::class, 'index'])->name('Vehicule.index');
Route::get('/Vehicule/create',   [VehiculeController::class, 'create'])->name('Vehicule.create');
Route::post('/Vehicule',   [VehiculeController::class, 'store'])->name('Vehicule.store');
Route::get('/Vehicule/{Vehicule}',   [VehiculeController::class, 'show'])->name('Vehicule.show');
Route::get('/Vehicule/{Vehicule}/edit',   [VehiculeController::class, 'edit'])->name('Vehicule.edit');
Route::put('/Vehicule/{Vehicule}',   [VehiculeController::class, 'update'])->name('Vehicule.update');
Route::get('/Vehicule/{Vehicule}',   [VehiculeController::class, 'destroy'])->name('Vehicule.destroy');



