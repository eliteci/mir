<!DOCTYPE html>
<html lang="en">
<head>
	<title>edit V17</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{asset("images/icons/favicon.ico ") }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href= "{{asset("vendor/bootstrap/css/bootstrap.min.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("fonts/font-awesome-4.7.0/css/font-awesome.min.css ")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("fonts/Linearicons-Free-v1.0.0/icon-font.min.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("vendor/animate/animate.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("vendor/css-hamburgers/hamburgers.min.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("vendor/animsition/css/animsition.min.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("vendor/select2/select2.min.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("vendor/daterangepicker/daterangepicker.css")}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset("css/util.css")}}">
	<link rel="stylesheet" type="text/css" href="{{asset("css/main.css")}}">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form action="{{ url('vehicule/' .$vehicule->id) }}" class="contact100-form validate-form" method="post" enctype="multipart/form-data">
			
			@csrf
			
				<span class="contact100-form-title">
					authentification
				</span>

				<label class="label-input100" for="cathégorie">categorie</label>
				<div class="wrap-input100" >
					<input id="categorie" class="input100" type="text" name="categorie" placeholder="categorie" value="{{ $vehicule->category }}">
					<span class="focus-input100"></span>
			
				</div>
				<label class="label-input100" for="numeromat">numero matricule</label>
				<div class="wrap-input100">
					<input id="numeromat" class="input100" type="text" name="numeromat" placeholder="Eg. +1 800 000000" value="{{ $vehicule->numeromat }}">
					<span class="focus-input100"></span>
				</div>

				<label class="label-input100" for="marque">marque</label>
				<div class="wrap-input100">
					<input id="marque" class="input100" type="text" name="marque" placeholder="jkhgtr" value="{{ $vehicule->marque }}">
					<span class="focus-input100"></span>
				</div>
				<label class="label-input100" for="model">modèle</label>
				<div class="wrap-input100">
					<input id="model" class="input100" type="text" name="model" placeholder="Eg. +1 800 000000" value="{{ $vehicule->model }}">
					<span class="focus-input100"></span>
				</div>
				<label class="label-input100" for="couleur">couleur</label>
				<div class="wrap-input100">
					<input id="couleur" class="input100" type="text" name="couleur" placeholder="bleu"value="{{ $vehicule->couleur }}">
					<span class="focus-input100"></span>
				</div>	
				<label class="label-input100" for="prix">prix</label>
				<div class="wrap-input100">
					<input id="prix" class="input100" type="text" name="prix" placeholder="1000fr" value="{{ $vehicule->prix }}">
					<span class="focus-input100"></span>
				</div>
				<label class="label-input100" for="annee">annee de fabrication</label>
				<div class="wrap-input100">
					<input id="anne" class="input100" type="text" name="anne" value="{{ $vehicule->anne }}" >
					<span class="focus-input100"></span>
				</div>
				<label class="label-input100" for="image">image</label>
				<div class="wrap-input100">
					<input id="image" class="input100" type="file" name="image" value="{{ $vehicule->image }}">
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<input type="submit" class="contact100-form-btn" value="add">
				</div>
			</form>









			<div class="contact100-more flex-col-c-m" style="background-image: url('images/bg-01.jpg');">
				<div class="flex-w size1 p-b-47">
					<div class="txt1 p-r-25">
						<span class="lnr lnr-map-marker"></span>
					</div>

					<div class="flex-col size2">
						<span class="txt1 p-b-20">
							Address
						</span>

						<span class="txt2">
							Mada Center 8th floor, 379 Hudson St, New York, NY 10018 US
						</span>
					</div>
				</div>

				<div class="dis-flex size1 p-b-47">
					<div class="txt1 p-r-25">
						<span class="lnr lnr-phone-handset"></span>
					</div>

					<div class="flex-col size2">
						<span class="txt1 p-b-20">
							Lets Talk
						</span>

						<span class="txt3">
							+1 800 1236879
						</span>
					</div>
				</div>

				<div class="dis-flex size1 p-b-47">
					<div class="txt1 p-r-25">
						<span class="lnr lnr-envelope"></span>
					</div>

					<div class="flex-col size2">
						<span class="txt1 p-b-20">
							General Support
						</span>

						<span class="txt3">
							contact@example.com
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="{{asset("vendor/jquery/jquery-3.2.1.min.js")}}"></script>
<!--===============================================================================================-->
	<script src="{{asset("vendor/animsition/js/animsition.min.js")}}"></script>
<!--===============================================================================================-->
	<script src="{{asset("vendor/bootstrap/js/popper.js")}}"></script>
	<script src="{{asset("vendor/bootstrap/js/popper.js")}}"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="{{asset("vendor/daterangepicker/moment.min.js")}}"></script>
	<script src="{{asset("vendor/daterangepicker/daterangepicker.js")}}"></script>
<!--===============================================================================================-->
	<script src="{{asset("vendor/countdowntime/countdowntime.js")}}"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src= "{{asset("https://www.googletagmanager.com/gtag/js?id=UA-23581568-13")}}"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
</body>
</html>
