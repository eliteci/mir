
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>vehicule</title>
    <link rel=" stylesheet" href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css')}}">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ asset('css/.css') }}">

    <style>
    .header {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        left: 0;
        right: 0;
        z-index: 1000;
        background: #fff;
        -webkit-box-shadow: 0 1rem 1rem rgba(0, 0, 0, 0.05);
                box-shadow: 0 1rem 1rem rgba(0, 0, 0, 0.05);
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
            -ms-flex-align: center;
                align-items: center;
        -webkit-box-pack: justify;
            -ms-flex-pack: justify;
                justify-content: space-between;
        padding: 2rem 9%;
        }
    .header .navbar a {
  font-size: 1.7rem;
  margin: 0 1rem;
  color: #666;
}

.header .navbar a:hover {
  color: #27ae60;
}
    </style>
     @livewireStyles
</head>

<body>


    <div class="header">
    <nav class="navbar">
     @foreach ($data as $vehicule)
   
     <ul>
     <li><a href="{{route('Vehicule.research')}}">{{$vehicule->marque}}</a></li>
  
     </ul>
      @endforeach
        <livewire:search/>
    </nav>
   

    </div>

     <div class="box-container" style="font-size=20px; ">
        @foreach ($data as $vehicule)
            <div class="box">
                <a href="#" class="fas fa-heart"></a>
                <div class="image">
                    <img src="/img/{{ $vehicule->image}}" alt="">
                </div>
                <div class="content">info
                    <h3>{{ $vehicule->model }}</h3>
                    <p>{{ $vehicule->marque }}</p>
                    <p>{{ $vehicule->numeromat }}</p>

                    <div class="price"><span>{{ $vehicule->prix }}</span></div>
                </div>
            </div>
        @endforeach
    </div>
 @livewireScripts
</body>

</html>