<?php

namespace App\Http\Livewire;
use App\Models\vehicule;

use Livewire\Component;

class Search extends Component
{   public $data=[];
    public string $query="";
    public function updatedQuery()

    {
        $word = '%' . $this->query . '%';
        if (strlen($this->query) >2){

            $this-> data = Vehicule::where('model', 'like', $word)
            ->orwhere('marque', 'like', $word)
            ->get();
        }
        // dd( $this-> data);
    }

    public function render()
    {
        return view('livewire.search');
    }
}
