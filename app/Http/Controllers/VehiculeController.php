<?php

namespace App\Http\Controllers;
use App\Models\vehicule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index()
    {
  
        $data = Vehicule::all();
        return view('index', compact('data'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data="";
        return view('create' , compact('vehicules'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  
    public function store(Request $request)
    {
        
        $input = $request->all();
         $data = Vehicule::all();

        $filename= time().  '.'  . $request->image->extension();
     

        $path = $request->file('image')->storeAs(
            'image' ,
            $filename,
            'public'
        );
 
        // $data->image = $path;
      
    //    $image->image=$data->image;
       
        // $name= Storage::disk('public')->put('image', $request->image);
     
       
     
        
      
 
        // dd($input);
        Vehicule::create($input);
       
        return redirect('/index')->with('flash_message', 'vehicule Addedd!');  
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data = "";
        return view('show',compact('data'));
      
        

    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Vehicule::find($id);
        return view('edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = "";
        return view('edit',compact('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Vehicule::find($id);
        $data->delete();

        return redirect()->back()->with('message','le produit a été supprimer');
    }

    public function research(Request $request ){

        $data = Vehicule::where('marque',$request->marque)->get();
        return view('allVehicule',compact('data'));
    }

}


 

    
  
    

 
  

 
  


